<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>GHANA INVESTMENT SUMMIT 2017</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>
<style type="text/css">
    .well {
        height:380px;
    }

    .form-group {
        margin-bottom: 0px;
    }
    .col-md-offset-2 {
        margin-left: 15.666667%;
    }
</style>

<!--<div class="row wrapper border-bottom white-bg page-heading" style="    margin-top: 6%;">
    <div class="col-md-5 col-md-offset-2 col-sm-5">
      
    </div>
</div>-->
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12" style="margin: 0px;padding: 0px;">
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="ibox-content" >
                    <div class="row">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-8">
                            <h2 style="    border-bottom: 1px solid #CCC; padding-bottom: 4%;">Thank you for the payment.</h2>
                            <div class="ibox-content p-xl" style="margin-bottom: 10%;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h5>From:</h5>
                                        <address>
                                            <strong>GHANA INVESTMENT SUMMIT 2017</strong><br>
                                        </address>
                                    </div>
                                    <div class="col-sm-6 text-right">                                       
                                        <h4>Order ID</h4>
                                        <h4 class="text-navy"><?php  echo $payment[0]->order_id; ?></h4>
                                        <p>
                                            <span><strong>Invoice Date:</strong> <?php echo date('M d, Y'); ?></span><br/>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h5>Name:</h5>
                                    </div>

                                    <div class="col-sm-6 text-right">
                                        <h5><?php echo $result[0]->name; ?></h5>
                                    </div>
                                </div>
                                <div class="row" style="background: #1AB394; padding-top: 15px; padding-bottom: 15px;">
                                    <div class="col-sm-6">
                                        <h5 style="color: #FFF;">Total Amount:</h5>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <h5 style="color: #FFF;"> GHS - <?php echo $payment[0]->total; ?></h5>
                                    </div>
                                </div>
                                <div><strong>Note: Email is sent to the registered id given</strong></div>
                            </div>

                           <div class="text-left col-sm-6 col-offset-l-0">
                                <a href="<?php echo base_url(); ?>" class="btn btn-warning"><i class="fa fa-angle-double-left"></i> Back to Home</a>
                            </div>
                            
                        </div>

                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>