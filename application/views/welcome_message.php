<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>GHANA INVESTMENT SUMMIT 2017</title>
         <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="http://localhost/wi/11-06-2017/resources/assets/css/style.css" rel="stylesheet">-->
        <link href="<?php echo base_url()."assets/style.css" ?>" rel="stylesheet">
        <style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
         
             .panel-body {
                    padding: 20px 40px 10px;
            }
            .error{
                color:#cc0000 !important;
                font-weight: 400 !important;
            }
             .required input:after { content:"*"; }
             
             
            .container {
                width: 100% !important;
/*                     padding-right: 0px !important; 
                     padding-left: 0px !important; */
            }
            body.apply {
                background: url(assets/img/gbt_background.jpg) no-repeat center top fixed;
                background-size: cover;
               
            }
            .color-r{
                 background: rgba(0, 0, 0, 0.5);
                 padding: 0px;
            }
            .header-logo-env {
                //background: rgba(255, 255, 255, 0.86);
                background: rgb(255, 255, 255);
            }
            .header-logo img {
                   width: 225px;
            }
            .header-logo {
                padding: 0px 100px; 
            }
            h2{
                color: #3e3e3e;
                font-size: 25px;
                line-height: 32px;
                font-style: normal;
                font-weight: 700;
            } 
            @media (max-width: 785px) { 
            
                .header-logo {
                    padding: 12px 15px;
                }
                .header-logo img {
                        width: 150px;
                }
                .panel-body {
                    padding: 15px 30px;
                }
            }
            
         </style>

</head>
<body class="apply">
<div class="container-fluid color-r">   
    <header class="header-logo-env">
        <div class="header-logo"><img src="assets/img/gbt_logo.png"></div>
    </header>
            <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-8 col-sm-offset-2 col-md-offset-2 border-cus" style="margin-top: 25px;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" name="register_form" action="<?php echo base_url(); ?>">
                                <div class="form-group">
                                    <b><h2>GHANA START-UP INVESTOR CONFERENCE</h2></b>
<p>THEME: Government's Role in building the right</p>
 
<p>ecosystem for start-ups and early stage businesses</p>

<p>The Ghana Start-up investor conference is cohosted by the Netherlands Ambassador to Ghana and Ghana's Trade Ministry to bring together Start-ups, SMEs, Impact Investors, business partners and government policy makers to learn, get connected and grow their businesses.</p>
                                  
</div>
                                
                                
                                
                                <div class="form-group">                                                                     
                                        <label>Your Name (required)</label>
                                       <input id="name" type="text" class="form-control" name="name">                                                                  
                                </div>

<!--                                <div class="form-group">                                   
                                        <label>Company</label>
                                        <input id="company" type="text" class="form-control" name="company">                                         
                                </div>-->

<!--                                <div class="form-group">
                                   
                                        <label>Position</label>
                                        <input id="text" type="text" class="form-control" name="position">                                       
                                   
                                </div>-->

<!--                                <div class="form-group">
                                   
                                        <label>Nature of Business</label>
                                        <input id="password-confirm" type="text" class="form-control" name="nature">                                     
                                   
                                </div>-->
                                
                                 <div class="form-group">
                                   
                                        <label>Your Email (required)</label>
                                        <input id="email" type="email" class="form-control" name="email">
                                       
                                   
                                </div>
                                
                                 <div class="form-group">
                                   
                                        <label>Phone Number</label>
                                        <input id="phone" type="text" class="form-control numbers-only mobile_num valid" name="phone">                                       
                                   
                                </div>
                                <div class="form-group">
                                    <label>GIS 2017 Registration Fee</label>
                                    <table class="table main-table_601" style="width: 100% !important;">
                                                 <tbody>
                                                    <tr>
                                                        <td class="row-qty" >
                                                           <select data-price="3353.55" name="750" class="price form-control Droppie">
                                                             <option value="0">0</option>
                                                             <option value="1">1</option>
                                                             <option value="2">2</option>
                                                             <option value="3">3</option>
                                                             <option value="4">4</option>
                                                             <option value="5">5</option>
                                                             <option value="6">6</option>
                                                             <option value="7">7</option>
                                                             <option value="8">8</option>
                                                             <option value="9">9</option>
                                                             <option value="10">10</option>
                                                          </select>
                                                       </td>
                                                       <td class="row-name">General Summit Registration</td>
                                                       <td class="row-price">3353.55 GHS (750.00 USD) </td>
                                                       
                                                    </tr>
                                                    <tr>
                                                       <td>
                                                          <select data-price="2235.7" name="500" class="price form-control Droppie">
                                                             <option value="0">0</option>
                                                             <option value="1">1</option>
                                                             <option value="2">2</option>
                                                             <option value="3">3</option>
                                                             <option value="4">4</option>
                                                             <option value="5">5</option>
                                                             <option value="6">6</option>
                                                             <option value="7">7</option>
                                                             <option value="8">8</option>
                                                             <option value="9">9</option>
                                                             <option value="10">10</option>
                                                          </select>
                                                       </td>
                                                       <td>Early Bird Special (ends June 30)</td>
                                                       <td>2235.7 GHS (500.00 USD)</td>
                                                       
                                                    </tr>
                                                    <tr>
                                                       <td>
                                                          <select data-price="1117.85" name="250" class="price form-control Droppie">
                                                             <option value="0">0</option>
                                                             <option value="1">1</option>
                                                             <option value="2">2</option>
                                                             <option value="3">3</option>
                                                             <option value="4">4</option>
                                                             <option value="5">5</option>
                                                             <option value="6">6</option>
                                                             <option value="7">7</option>
                                                             <option value="8">8</option>
                                                             <option value="9">9</option>
                                                             <option value="10">10</option>
                                                          </select>
                                                       </td>
                                                       <td>Fundraising Dinner Gala</td>
                                                       <td>1117.85 GHS (250 USD)</td>
                                                    </tr>

                                                      <tr>
                                                          <td colspan="3">
                                                              Total Amount:<span id="totalText" ></span> GHS
                                                              
                                                          </td>
                                                      </tr>
               </tbody>
            </table>
                                    
                                    
                                    
                                </div>
                                

<!--                                
                                <div class="form-group">
                                   
                                        <label>Post Code</label>
                                        <input id="post_code" type="text" class="form-control" name="post_code">
                                       
                                    
                                </div>-->
                                
                                  
<!--                                <div class="form-group">
                                    
                                        <label>Country</label>
                                        <input id="country" type="text" class="form-control" name="country">                                     
                                   
                                </div> -->
                                <div class="form-group">                                    
                                        <label>Captcha</label>
                                        <br/>How much is: <input type="text" id="a" readonly="readonly" style="border:1px solid #FFF;"/>
                                        <br/>Answer:<input type="text" class="form-control" id="b"/>                                                     
                                </div>  
                               
                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-md-12"> 
                                        <input type="submit" value="Submit" class="btn btn-primary" id="register_btn" name="submit"/>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        
            </div>
            </div>

   <script type="text/javascript" src="http://tongaa.com/assets/tonga-content/jquery.min.js"></script>
<script type="text/javascript" src="http://tongaa.com/assets/tonga-content/jquery.validate.min.js"></script>

    
    <script type="text/javascript">
$(document).ready(function()
{
    $('#totalText').text((0).toFixed(2)); 
    $("#register_btn").removeAttr('disabled');
    var form = $("[name=register_form]");
    form.validate(
    {
        ignore: "",
        rules:
        {
            name:
            {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            company:
            {
                required: true                
            },
            position:
            {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            nature:
            {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            email:
            {
                required: true,
                email:true,
                maxlength: 100
            },
            phone:
            {
                required: true,
                number: true,
                minlength:10,
                maxlength:10
            },
            post_code:
            {
                required: true,
                number: true,
                minlength:5
            },
            country:
            {
                required: true,
                maxlength: 100,
                minlength:2
            }
        },
        messages:
        {
            name:
            {
                required: "Please Enter name"
            },
            company:
            {
                required: "Please Enter company"
            },
            position:
            {
                required: "Please Enter position"
            },
            nature:
            {
                required: "Please Enter nature"
            },
            email:
            {
                required:"Please Enter Email Id",
                email:"Please Enter proper Email Id"
            },
            phone:{
                required:"Please Enter phone number"
            },
            post_code:
            {
                required:"Please Enter post code"
            },
            country:
            {
                required: "Please Enter country"
            }
        }
    });
});

 var select_name = '';
        var ab = [];
        $('.price').change(function () {      
            var tot = 0;
            var val = parseFloat(this.value) * parseFloat($(this).attr('data-price'));
                        
            var check = [];
            var select_name = this.name;
            ab[select_name] = val;
                 
            $.each( ab, function( index, value ) {
             if(value){
                 tot = value+tot; 
             }
  
            }); 
            $('#totalText').text((tot).toFixed(2));                
        });
    
    
    
    
    $('#register_btn').click(function() {
    if(SaveCheck()) {
     //   alert('successfull');
    }
    else {
        alert("Sorry, at least one select ticket.");
    }
});

function SaveCheck() {
    var flag = false;
    $('.Droppie :selected').each(function() {
        if($(this).val() == "1" || $(this).val() == "2" || $(this).val() == "3" || $(this).val() == "4" || $(this).val() == "5" || $(this).val() == "6" ||$(this).val() == "7" || $(this).val() == "8" || $(this).val() == "9" || $(this).val() == "10" ) {
            flag = true;
            return false;
        }
    });
    return flag;
    }
    
    
    
    $(document).ready(function() {
    var n1 = Math.round(Math.random() * 10 + 1);
    var n2 = Math.round(Math.random() * 10 + 1);
    $("#a").val(n1 + " + " + n2);
    $("#register_btn").click(function() {
        if (eval($("#a").val()) == $("#b").val()) {
           
        } else {
            alert("Please Enter correct value in captcha!");
             return false;
        }
    });
});

// captcha hack

    </script>
</body>
</html>