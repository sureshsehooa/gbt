<?php date_default_timezone_set('Asia/Calcutta'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Email</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    </head>
    <body style="padding:0; margin:0;">
        <div  style="width: 800px; background: #f8f8f8; margin:0 auto;
              font-family: Arial,Tahoma,Verdana,sans-serif;
              font-size:16px;
              -webkit-text-size-adjust: 100% !important;
              -ms-text-size-adjust: 100% !important;
              -webkit-font-smoothing: antialiased !important; padding:50px;">
            <div style="background: #fff;">
                <div id="header" class="bg-eee" style="text-align: center; min-height: 76px; width: 100%; background-color: #333;">
                    <div id="logo">
                        <h3 style='color:#fff;padding-top: 20px;font-size:25px;font-family: "Helvetica Neue","Segoe UI",Segoe,Helvetica,Arial,"Lucida Grande",sans-serif; margin-top:0'>GHANA INVESTMENT SUMMIT 2017</h3>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="content" style="margin-top:20px; padding: 28px 47px; overflow: hidden; width:700px; min-height:
                     300px;">

                    Dear <?php echo $user[0]->name; ?> ,<br /><br />
                    
                    <p>Thank you for showing interest. Please find your details below:</p>
                    
                     
                       
                     <br/>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">                      
                        <tbody> 
                             <tr>
                                   <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-family:Arial,Helvetica,sans-serif" colspan="4"> <h4>DETAILS</h4></td>
                    </tr>   
                        <tr>
                                   <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Order Id</b></td>
                                <td colspan="3" style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $order[0]->order_id; ?></td> 
                               </tr>
                            <tr>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Name</b></td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $user[0]->name; ?></td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Email Id</b></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo  $user[0]->email; ?></td>    
                            </tr>
                           
                            <tr>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Phone Number</b></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif" colspan="3"><?php echo $user[0]->phone; ?></td>
<!--                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Nature of Business</b></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php // echo  $user[0]->nature; ?></td>    -->
                            </tr>
                           
                            
<!--                             <tr>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Email</b></td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php //echo $user[0]->email; ?></td> 
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Phone Number</b></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php //echo $user[0]->phone; ?></td>    
                            </tr>
                               
                               <tr>
                                   <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Post Code</b> </td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php //echo $user[0]->post_code; ?></td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><b>Country</b></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php //echo $user[0]->country; ?></td>    
                            </tr>-->
                            
                        </tbody>
                    </table>
                    
                    <br /><br />
                    
                    
                    
                    
                    
                    
                  
                     
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">                      
                        <tbody>
                           <tr>
                               <th style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif" colspan="4"><h4>TICKETS DETAILS</h4></th>
                            </tr>
                            <tr>
                                <th style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">Name Of Ticket</th>
                                <th style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">Per Ticket(GHS)</th>
                                <th style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">No.of Tickets</th>
                                <th style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">Total(GHS)</th>
                            </tr>
                            <?php if($result[0]->no_ticket > 0){?>
                            <tr>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">General Summit Registration </td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[0]->per_price ?></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[0]->no_ticket ?></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo @$result[0]->no_ticket * @$result[0]->per_price; ?></td>    
                            </tr>
                            <?php } 
                              if($result[1]->no_ticket > 0){?>
                            <tr>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">Early Bird Special (ends June 30)</td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[1]->per_price ?></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[1]->no_ticket ?></td>   
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo @$result[1]->no_ticket * @$result[1]->per_price; ?></td> 
                            </tr>
                              <?php } 
                               if($result[2]->no_ticket > 0){
                              ?>
                            <tr>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif">Fundraising Dinner Gala</td>
                                 <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[2]->per_price ?></td>
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo $result[2]->no_ticket ?></td>      
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo @$result[2]->no_ticket * @$result[2]->per_price; ?></td> 
                            </tr>
                               <?php } ?>
                               <tr>
                                   <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif" colspan="3">Grand Total(GHS)</td>
                                
                                <td style="padding: 8px 5px;border: 1px solid #e5e5e5;font-size: 12px;font-size:1em;line-height:22px;font-family:Arial,Helvetica,sans-serif"><?php echo (@$result[0]->no_ticket* @$result[0]->per_price) + (@$result[1]->no_ticket * @$result[1]->per_price) + (@$result[2]->no_ticket * @$result[2]->per_price) ?></td>                                                                
                            </tr>
                            
                        </tbody>
                    </table>
                      
                     
                    
                    
                    
                    <br />
                    <br />
                    
                </div>
                <div style="clear: both;"></div>
                <div id="footer" align="center" style="margin-top:30px;width: 100%; margin-top:30px;">
                    <div id="social-media" style="background: #333;padding: 12px 0px;font-size: 13px; color: #fff;">
                        <div id="social-media-icon-container">
                            <div style="float:left; text-align: left; padding-left:30px;">
                                Copyrights &copy; 2015 - <?= date('Y') ?>
                            </div>
                            <div style="float:right; text-align: right; padding-right:30px;">
                                Powered by Interpay &amp; Aavidcode
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>